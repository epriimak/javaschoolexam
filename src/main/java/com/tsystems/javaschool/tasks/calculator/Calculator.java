package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private static String operators = "+-*/u";
    private static String delimiters = " ()" + operators;
    private static String digits = "0123456789";

    private static boolean isDelimiter(String token) {
        return delimiters.contains(token);
    }

    private static boolean isOperator(String token) {
        return operators.contains(token);
    }

    private static boolean isDigit(String token) {
        return digits.contains(token);
    }

    private static int priority(String token) {
        switch (token) {
            case "(":
                return 1;
            case "+":
            case "-":
                return 2;
            case "*":
            case "/":
                return 3;
            default:
                return 4;

        }
    }

    private static List<String> parseInput(String input) {
        List<String> outputStack = new ArrayList<>();
        Deque<String> opStack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(input, delimiters, true);

        String prev = "";
        String cur;

        while (tokenizer.hasMoreTokens()) {
            cur = tokenizer.nextToken();
            if (!tokenizer.hasMoreTokens() && isOperator(cur))
                return null;

            if (cur.equals(" "))
                continue;

            else if (cur.equals("("))
                opStack.push(cur);

            else if (cur.equals(")")) {
                while (!opStack.peek().equals("(")) {
                    outputStack.add(opStack.pop());
                    if (opStack.isEmpty())
                        return null;
                }
                opStack.pop();
            }

            else if (isOperator(cur)) {
                if (isOperator(prev))
                    return null;

                if (cur.equals("-") && (prev.equals("") || (isDelimiter(prev) && !prev.equals(")")))) {
                    cur = "u";
                } else {
                    while (!opStack.isEmpty() && (priority(cur) <= priority(opStack.peek()))) {
                        outputStack.add(opStack.pop());
                    }
                }
                opStack.push(cur);
            } else {
                outputStack.add(cur);
            }
            prev = cur;
        }

        while (!opStack.isEmpty()) {
            if (isOperator(opStack.peek()))
                outputStack.add(opStack.pop());
            else
                return null;
        }
        return outputStack;
    }


    public static Double calcExpr(List<String> outputStack) {
        if (outputStack == null)
            return null;

        Deque<Double> stack = new ArrayDeque<>();

        for (String s : outputStack) {
            if (s.equals("+"))
                stack.push(stack.pop() + stack.pop());

            else if (s.equals("-"))
                stack.push(-(stack.pop() - stack.pop()));

            else if (s.equals("*"))
                stack.push(stack.pop() * stack.pop());

            else if (s.equals("/")) {
                Double b = stack.pop();
                Double a = stack.pop();

                if (b == 0)
                    return null;
                stack.push(a / b);
            }

            else if (s.equals("u"))
                stack.push(-stack.pop());

            else
                try {
                    BigDecimal bd = new BigDecimal(s).setScale(4, RoundingMode.HALF_EVEN);
                    Double d = bd.doubleValue();
                    stack.push(d);
                } catch (NumberFormatException e) {
                    return null;
                }
        }

        return stack.pop();
    }

    public String evaluate(String statement) {
        if (statement == null || statement.length() == 0)
            return null;

        List<String> expression = parseInput(statement);
        Double res = calcExpr(expression);
        if (res == null)
            return null;

        else if (res % 1 == 0)
            return String.valueOf(res.intValue());

        else return res.toString();
    }
}
