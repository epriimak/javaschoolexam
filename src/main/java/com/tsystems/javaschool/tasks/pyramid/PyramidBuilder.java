package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if(inputNumbers == null || inputNumbers.size() == 0 || inputNumbers.size() >= Integer.MAX_VALUE - 1 || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        List<Integer> sortedInputNumbers = new ArrayList<>(inputNumbers);
        sortedInputNumbers.sort(Comparator.comparing(Integer::valueOf));

        int size = sortedInputNumbers.size();
        int numCols = 1;
        int numRows = 1;
        int count = 0;

        while (count < size) {
            count += numRows;
            numRows++;
            numCols += 2;
        }

        if(count != size)
            throw new CannotBuildPyramidException();

        numRows -= 1;
        numCols -= 2;

        int res[][] = new int[numRows][numCols];

        int jInd = numCols / 2;
        int iInd = 0;
        count = 0;

        for(; iInd < numRows; iInd++){
            int curJPos = jInd;
            int countRow = 1;
            while (countRow <= iInd + 1){
                res[iInd][curJPos] = sortedInputNumbers.get(count++);
                curJPos += 2;
                countRow++;
            }

            jInd = jInd - 1;
        }
        return res;
    }
}
